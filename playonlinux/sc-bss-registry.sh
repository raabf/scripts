#!/bin/bash
#
# Only For : http://www.playonlinux.com
# Date : (2012-05-30 22-00)
# Last revision : (2012-05-30 22-00)
# Wine version used : 1.5.3
# Distribution used to test : Ubuntu 12.04 LTS
# Author : Amyros 
# E-Mail : amyros@live.de

[ "$PLAYONLINUX" = "" ] && exit 0
source "$PLAYONLINUX/lib/sources"

TITLE="Starcraft and Brood War - Best Seller Series"
PREFIX="Starcraft"
WORKING_WINE_VERSION="1.5.3"

POL_SetupWindow_Init
POL_Debug_Init

POL_SetupWindow_presentation "$TITLE" "Blizzard" "http://www.blizzard.com/games/sc/" "Amyros <amyros@live.de>" "$PREFIX"
POL_System_wait "$TXT_wait" "$TITLE"

# check if all needed programms are instlled
check_one "sed" "sed"
check_one "wget" "wget"
POL_SetupWindow_Missing

sc_InstallPath_Std_Win="c:\Program Files\StarCraft"

if [ "$POL_LANG" == "de" ]

then # German translation
TXT_wait="Bitte warten ..."
TXT_SetupFinished="$TITLE wurde erfolgreich installiert."
TXT_SelectSetupFile="Bitte wählen sie die Installations Datei, die sie ausführen wollen:"
TXT_Download_Bnet_patch="Battle.net Patch wird heruntergeladen ..."
TXT_On_Desktop="Es gibt oft Probleme mit neuen hochauflösenden Bildschirmen, besonders mit breitbild Bildschirmen. \nWollen sie den virtuellen Desktop im Fenstermodus aktivieren (empfolen)? \n\nFalls es nicht funktioniert, wählen sie ihre $PREFIX Verknüpfung im PlayOnLinux Fenster, klicken sie auf Konfigurieren >> Wine >> Wine konfigurieren >> Grafik und aktivieren sie den virtuellen Desktop mit 800x600."
TXT_Install_Patch="Es wird empfolen den neuersten Patch zu installieren. \nKlicken Sie dazu im PlayOnLinux Fester auf Installiern und wählen Sie unter Korrekturen einen passenden $PREFIX patch wie Starcraft and Brood War - Patcher."

else # all other Languages or Englisch
TXT_wait="$(eval_gettext 'Please wait ...')"
TXT_SetupFinished="$(eval_gettext '$TITLE has been successfully installed.')"
TXT_SelectSetupFile="$(eval_gettext 'Please select the setup file to run.')"
TXT_Download_Bnet_patch="$(eval_gettext 'Downloading Battle.net Patch ...')"
TXT_On_Desktop="$(eval_gettext 'There are often Problems with new high resolution displays, especially wide screen displays. \nSo do you want to activate the virtual Desktop in window-mode (recommend)? \n\nIf it do not work, select your $PREFIX starter in the PlayOnLinux Window, klick on configuration >> Wine >> configure Wine >> Graphics and enable virtual Display to 800x600.')"
TXT_Install_Patch="$(eval_gettext 'It is recommend to install the newest patch. \nTo do this klick on Installation in your PlayOnLinux Window and choose under the Patches section an appropiate patch for $PREFIX like Starcraft and Brood War - Patcher.')"
fi

POL_Wine_SelectPrefix "$PREFIX"
POL_Wine_PrefixCreate "$WORKING_WINE_VERSION"

POL_System_TmpCreate "$PREFIX"

# If the installer Crashes on previous Installations and the TmpDir exists already
POL_System_TmpDelete
POL_System_TmpCreate "$PREFIX"

##
# needed:
# returns: Unix2Win_Path_RET
##
function Unix2Win_Path() { # $1= Unix Path: String $2= Prefix: String (default "z:\")

POL_Debug_Message "Unix path: $1"
POL_Debug_Message "returned path Prefix: $2"

if [ "$2" = "" ]
then
	Unix2Win_Path_RET="z:$(echo $1 | sed -e 's@\/\/@\/@g;s@\/@\x5c@g;')"
else
	Unix2Win_Path_RET="$2:$(echo $1 | sed -e 's@\/\/@\/@g;s@\/@\x5c@g;')"
fi

POL_Debug_Message "returend Win path: $Unix2Win_Path_RET"

}

##
# needed:
# returns: Win2Unix_Path_RET
##
function Win2Unix_Path() { # $1= Win Path: String $2= returend path Prefix: String (default "/")
POL_Debug_Message "Win path: $1"
POL_Debug_Message "returned path Prefix: $2"

if [ "$2" = "" ]

then # default prefix "/"
# sed commande changes \ to / and eliminates doubles // to /
	Win2Unix_Path_RET="$(echo "$1" | sed -e 's@[\x5c]@\/@g;s@[a-zA-Z]:@@g;s@\/\{1,6\}@\/@g;')"
	
else
	Win2Unix_Path_RET="$2$(echo "$1" | sed -e 's@[\x5c]@\/@g;s@[a-zA-Z]:@@g;s@\/\{1,6\}@\/@g;')"
fi

POL_Debug_Message "returend unix path: $Win2Unix_Path_RET"

}

# get Win Path of the Temp Dir
Unix2Win_Path "$POL_System_TmpDir"
POL_System_TmpDir_Win=$Unix2Win_Path_RET
POL_Debug_Message "POL_System_TmpDir_Win= $POL_System_TmpDir_Win"

##
# Gets the Data of a value in a Windows Registry Key
#
# needed: POL_System_TmpDir_Win, POL_System_TmpDir
# returns: getRegValue_RET
##
POL_Debug_Message "Start getRegValue()"
function getRegValue() { # $1=Value: String  $2=Registry Key: String $3= Standart value on fail

POL_Debug_Message "Value to get: $1"
POL_Debug_Message "Key: $2"

# read registry
cd $POL_System_TmpDir
POL_Wine regedit /e "$POL_System_TmpDir_Win\val_$1.reg" "$2"

if [ "$(cat val_$1.reg)" = "" -o ! -f "val_$1.reg" ]

then #if key was not sucessful exported
	POL_Debug_Error "$PREFIX registry key for reading \"$1\" was not sucessful exported! Is $PREFIX correctly installed? Standart value ($3) is used."

getRegValue_RET=$3
POL_Debug_Message "returend value data (standart value): $getRegValue_RET"

else
POL_Debug_Message "Save Key to $POL_System_TmpDir_Win\val_$1.reg"

# first sed kommando gets the line the second extract the data and
# change \\ to \
getRegValue_RET=$(sed -n -e '/'$1'/p' "$POL_System_TmpDir/val_$1.reg" | sed -e 's@"'$1'"="\(.*\)".*@\1@g;s@[\x5c][\x5c]@\x5c@g;')
POL_Debug_Message "returend value data: $getRegValue_RET"

fi
}


POL_SetupWindow_InstallMethod "LOCAL,CD"
if [ $INSTALL_METHOD = "LOCAL" ]
	then
	POL_SetupWindow_browse "$TXT_SelectSetupFile" "$TITLE"
	
	POL_Wine start /unix "$APP_ANSWER"
	POL_Wine_WaitExit "$PREFIX"

elif [ $INSTALL_METHOD = "CD" ]
	then
		
	POL_SetupWindow_cdrom
	POL_SetupWindow_check_cdrom "StarCraft (Windows).exe"
	
	POL_Wine start /unix "$CDROM/StarCraft (Windows).exe"
	POL_Wine_WaitExit "$PREFIX"

else
	POL_Debug_Fatal "Error: Unexpected Return value of the InstallMethod Window."

fi

POL_SetupWindow_wait "$TXT_wait" "$TITLE"

POL_Shortcut "StarCraft.exe" "Starcraft & Brood War"

# gets the install Path of Starcraft from registry and sets varaibles
getRegValue "InstallPath" "HKEY_LOCAL_MACHINE\Software\Blizzard Entertainment\Starcraft" "$sc_InstallPath_Std_Win"
SC_InstallPath_Win="$getRegValue_RET"
POL_Debug_Message "SC_InstallPath_Win=$SC_InstallPath_Win"

Win2Unix_Path "$SC_InstallPath_Win" "$WINEPREFIX/drive_c"
SC_InstallPath_Unix="$Win2Unix_Path_RET"
POL_Debug_Message "SC_InstallPath_Unix=$SC_InstallPath_Unix"

cd "$SC_InstallPath_Unix"
POL_Download "http://files.playonlinux.com/ddraw.dll"

POL_SetupWindow_question "$TXT_On_Desktop" "$TITLE"
if [ $APP_ANSWER = TRUE ]
then
	POL_SetupWindow_wait "$TXT_wait" "$TITLE"
	POL_Debug_Message "Answer Yes: Set_Desktop On 800 600"
	Set_Desktop "On" "800" "600"
fi


POL_SetupWindow_wait "$TXT_wait" "$TITLE"
POL_SetupWindow_message "$TXT_SetupFinished\n\n$TXT_Install_Patch" "$TITLE"
POL_System_TmpDelete
POL_SetupWindow_Close