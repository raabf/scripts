#!/bin/bash
#
# Only For : http://www.playonlinux.com
# Date : (2012-05-30 22-00)
# Last revision : (2012-08-15 22-00)
# Wine version used : 1.4 for installing, 1.1.38 for playinf
# Distribution used to test : Ubuntu 12.04 LTS
# Author : Amyros 
# E-Mail : amyros@live.de
# Script licence : no licence
# Program licence : proprietary
# Depend : -

[ "$PLAYONLINUX" = "" ] && exit 0
source "$PLAYONLINUX/lib/sources"

TITLE="Starcraft and Brood War - Best Seller Series"
PREFIX="Starcraft"

# In my Tests the Discs could only be detected by wine version 1.4 or higher
# but the Application works better with a lower version
WORKING_INSTALL_WINE_VERSION="1.4"
WORKING_WINE_VERSION="1.1.38"

POL_GetSetupImages "http://img715.imageshack.us/img715/7455/topdz.jpg" "http://img32.imageshack.us/img32/1266/leftjo.jpg" "$TITLE"

POL_SetupWindow_Init
POL_Debug_Init

POL_SetupWindow_presentation "$TITLE" "Blizzard" "http://www.blizzard.com/games/sc/" "Amyros <amyros@live.de>" "$PREFIX"
POL_System_wait "$TXT_wait" "$TITLE"

# check if all needed programms are instlled
POL_SetupWindow_Missing

sc_InstallPath_Std="/Program Files/StarCraft/"

if [ "$POL_LANG" == "de" ]

then # German translation
TXT_wait="Bitte warten ..."
TXT_SetupFinished="$TITLE wurde erfolgreich installiert."
TXT_SelectSetupFile="Bitte wählen sie die Installations Datei, die sie ausführen wollen:"
TXT_Download_Bnet_patch="Battle.net Patch wird heruntergeladen ..."
TXT_On_Desktop="Es gibt oft Probleme mit neuen hochauflösenden Bildschirmen, besonders mit breitbild Bildschirmen. \nWollen sie den virtuellen Desktop im Fenstermodus aktivieren (empfolen)? \n\nFalls es nicht funktioniert, wählen sie ihre $PREFIX Verknüpfung im PlayOnLinux Fenster, klicken sie auf Konfigurieren >> Wine >> Wine konfigurieren >> Grafik und aktivieren sie den virtuellen Desktop mit 800x600."
TXT_exp_cd="Sie haben die \"expension set\" CD eingelegt. Bitte legen Sie zuerst die andere CD ein und klicken Sie auf Weiter. Legen Sie die \"expension set\" CD ein, wenn Sie während der Installation aufgefordert werden Disc 2 einzulegen."

else # all other Languages or Englisch
TXT_wait="$(eval_gettext 'Please wait ...')"
TXT_SetupFinished="$(eval_gettext '$TITLE has been successfully installed.')"
TXT_SelectSetupFile="$(eval_gettext 'Please select the setup file to run.')"
TXT_Download_Bnet_patch="$(eval_gettext 'Downloading Battle.net Patch ...')"
TXT_On_Desktop="$(eval_gettext 'There are often Problems with new high resolution displays, especially wide screen displays. \nSo do you want to activate the virtual Desktop in window-mode (recommend)? \n\nIf it do not work, select your $PREFIX starter in the PlayOnLinux Window, klick on configuration >> Wine >> configure Wine >> Graphics and enable virtual Display to 800x600.')"
TXT_exp_cd="$(eval_gettext 'You have inserted the \"expension set\" CD. PLease insert the other CD first then klick on next. Please insert the \"expension set\" CD when you are promted for Disc 2 during setup.')"
fi

POL_Wine_SelectPrefix "$PREFIX"
POL_Wine_PrefixCreate "$WORKING_INSTALL_WINE_VERSION"

POL_System_TmpCreate "$PREFIX"

POL_SetupWindow_InstallMethod "LOCAL,CD"
if [ $INSTALL_METHOD = "LOCAL" ]
	then
	POL_SetupWindow_browse "$TXT_SelectSetupFile" "$TITLE"
	
	POL_Wine start /unix "$APP_ANSWER"
	POL_Wine_WaitExit "$PREFIX"

elif [ $INSTALL_METHOD = "CD" ]
	then
		
	POL_SetupWindow_cdrom
	POL_SetupWindow_check_cdrom "StarCraft (Windows).exe"
	
	# check if expension cd is inserted
	if [ -f "$CDROM/Installer Tome 2.mpq" ]; then
	
		POL_SetupWindow_message "$TXT_exp_cd" "$TITLE"
		POL_SetupWindow_cdrom
		
	fi
	
	#check CD1 files again
	POL_SetupWindow_check_cdrom "Installer Tome.mpq"
	POL_SetupWindow_check_cdrom "StarCraft (Windows).exe"
	
	POL_Wine start /unix "$CDROM/StarCraft (Windows).exe"
	POL_Wine_WaitExit "$PREFIX"
else
	POL_Debug_Fatal "Error: Unexpected Return value of the InstallMethod Window."

fi

POL_SetupWindow_wait "$TXT_wait" "$TITLE"

sc_InstallPath="$WINEPREFIX/drive_c$sc_InstallPath_Std"
POL_Debug_Message "sc_InstallPath_Std= $sc_InstallPath_Std"

# check if Starcraft is installed in the standart directory /Program Files/StarCraft/, else promt for the install directory
while [ ! -f "${sc_InstallPath}StarCraft.exe" ]; do

POL_Debug_Message "Selected Path: $APP_ANSWER"
POL_SetupWindow_browse "Starcraft was not found in the standart folder. Please select your StarCraft.exe in your installation folder:" "$TITLE"
sc_InstallPath="${APP_ANSWER:0:-13}"
POL_Debug_Message "sc_InstallPath= $sc_InstallPath"

done

POL_SetupWindow_wait "$TXT_wait" "$TITLE"

POL_Shortcut "StarCraft.exe" "Starcraft & Brood War"

# download battle.net fix
cd "$sc_InstallPath"
POL_Download "http://files.playonlinux.com/ddraw.dll" "efe4aa40d8633213dbfd2c927c7c70b0"

POL_Wine_PrefixCreate "$WORKING_WINE_VERSION"

# ask for virtual desktop activation
POL_SetupWindow_question "$TXT_On_Desktop" "$TITLE"
if [ $APP_ANSWER = TRUE ]
then
	POL_SetupWindow_wait "$TXT_wait" "$TITLE"
	POL_Debug_Message "Answer Yes: Set_Desktop On 800 600"
	Set_Desktop "On" "800" "600"
fi


POL_SetupWindow_wait "$TXT_wait" "$TITLE"
POL_SetupWindow_message "$TXT_SetupFinished" "$TITLE"
POL_System_TmpDelete
POL_SetupWindow_Close
