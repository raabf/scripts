#!/bin/bash
# Changelog
#
# [Quentin Pâris] (2011-10-29 19:52) Translation updated
#    Changed prefix name
#    Added debugging
#
# [SuperPlumus] (2011-10-29 19:46) Updated for pol 4
#    rm -rf "$WINEPREFIX/drive_c/windows/temp/*" with quote won't remove anything
#    POL_Function_FontsSmoothRGB replace by POL_Call POL_Function_FontsSmoothRGB
# 
# Date : (2009-09-12 10-15)
# Last revision : (2011-08-10 13:39)
# Wine version used : 1.2.1, 1.2.3
# Distribution used to test : Debian Testing x64
# Author : MulX
# Licence : Retail
# Only For : http://www.playonlinux.com
  
[ "$PLAYONLINUX" = "" ] && exit 0
source "$PLAYONLINUX/lib/sources"
  
TITLE="Age Of Empires II : The Age of Kings"
PREFIX="AgeOfEmpire2"
WORKING_WINE_VERSION="1.4-rc1"
GAME_VMS="64"
 
#starting the script
POL_SetupWindow_Init
POL_Debug_Init
 
POL_SetupWindow_presentation "$TITLE" "Microsoft Games" "http://www.microsoft.com" "MulX" "$PREFIX"
  
# Setting prefix path
POL_Wine_SelectPrefix "$PREFIX"
 
# Downloading wine if necessary and creating prefix
POL_System_SetArch "auto"
POL_Wine_PrefixCreate "$WORKING_WINE_VERSION"
 
POL_System_TmpCreate "$PREFIX"
 
# Installing mandatory components
POL_Wine_InstallFonts
POL_Call POL_Function_FontsSmoothRGB
 
Set_OS "win2k"
 
#Choose between CD and Digital Download version
POL_SetupWindow_InstallMethod "CD,LOCAL"
 
# Begin game installation
if [ "$INSTALL_METHOD" = "CD" ]; then
        # Asking for CDROM and checking if it's correct one
        POL_SetupWindow_message "$(eval_gettext 'Please insert the game media into your disk drive.')" "$TITLE"
        POL_SetupWindow_cdrom
        POL_SetupWindow_check_cdrom "aoesetup.exe"
        POL_Wine start /unix "$CDROM/aoesetup.exe"
        POL_Wine_WaitExit "$TITLE"
else
        # Asking then installing DDV of the game
        cd "$HOME"
        POL_SetupWindow_browse "$(eval_gettext 'Please select the setup file to run')" "$TITLE"
        SETUP_EXE="$APP_ANSWER"
        POL_SetupWindow_wait "$(eval_gettext 'Please wait while $TITLE is installed.')" "$TITLE"
        POL_Wine start /unix "$SETUP_EXE"
        POL_Wine_WaitExit "$TITLE"
fi
 
# Asking about memory size of graphic card
POL_SetupWindow_VMS $GAME_VMS
 
## Fix for this game No longer needed with recent wine
Set_Desktop On 1024 768
 
cd "$POL_System_TmpDir" || POL_Debug_Fatal "Unable to change directory"
 
POL_Download "http://www.thehandofagony.com/alex/dll/dplaydlls-win98se.tar.bz2" "2cc36b915b901e7656ad3b533f83aa7d"
tar -jxvf "dplaydlls-win98se.tar.bz2"
rm "$WINEPREFIX/drive_c/windows/system32/dplayx.dll"
rm "$WINEPREFIX/drive_c/windows/system32/dpnet.dll"
rm "$WINEPREFIX/drive_c/windows/system32/dpnhpast.dll"
rm "$WINEPREFIX/drive_c/windows/system32/dpwsockx.dll"
mv *.dll "$WINEPREFIX/drive_c/windows/system32/"
 
POL_Wine_OverrideDLL "native,builtin" "dplayx"
POL_Wine_OverrideDLL "native,builtin" "dpnet"
POL_Wine_OverrideDLL "native,builtin" "dpnhpast"
POL_Wine_OverrideDLL "native,builtin" "dpwsockx"
 
# Sound problem fix - pulseaudio related
[ "$POL_OS" = "Linux" ] && Set_SoundDriver "alsa"
[ "$POL_OS" = "Linux" ] && Set_SoundEmulDriver "Y"
## End Fix
 
POL_System_TmpDelete
 
# Making shortcut
POL_Shortcut "empires2.exe" "$TITLE" "AgeOfEmpires.xpm" "-nostartup"
 
POL_SetupWindow_Close
exit 0
