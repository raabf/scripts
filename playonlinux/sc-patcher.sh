#!/bin/bash
#
# Only For : http://www.playonlinux.com
# Date : (2012-05-30 22-00)
# Last revision : (2012-05-30 22-00)
# Wine version used : 1.5.3
# Distribution used to test : Ubuntu 12.04 LTS
# Author : Amyros 
# E-Mail : amyros@live.de

[ "$PLAYONLINUX" = "" ] && exit 0
source "$PLAYONLINUX/lib/sources"

TITLE="Starcraft and Brood War - Patcher"
PREFIX="Starcraft"

POL_SetupWindow_Init
POL_Debug_Init

POL_SetupWindow_presentation "$TITLE" "Blizzard" "http://www.blizzard.com/games/dc/" "Amyros <amyros@live.de>" "$PREFIX"


if [ "$POL_LANG" == "de" ]

then # German translation
TXT_downloadInstallPath_wait="Bitte warten ..."
TXT_SetupFinished="$TITLE wurde erfolgreich installiert."
TXT_SelectSetupFile="Bitte wählen sie die Installations Datei, die sie ausführen wollen:"
TXT_Fatal_NoPrefix="Fehler: Kein $PREFIX Wineprefix gefunden. Haben Sie $PREFIX korrekt installiert?"
TXT_auto_detect_patch_BW_1="Starcraft und Starcraft: Brood War wurde auf ihrem Computer gefunden.\nWollen sie den aktuellsten Patch v"
TXT_auto_detect_patch_BW_2=" installieren [Ja] oder wollen Sie die Version des Patches selbst von einer Liste wählen [Nein]?"

TXT_auto_detect_patch_SC_1="Starcraft (ohne Erweiterungen) wurde auf ihrem Computer gefunden.\nWollen sie den aktuellsten Patch v"
TXT_auto_detect_patch_SC_2=" installieren [Ja] oder wollen Sie die Version des Patches selbst von einer Liste wählen [Nein]?"
TXT_choose_version_select="Bitte wählen Sie die Version, die Sie installieren möchten:"
TXT_choose_version_select_SC="Bitte wählen Sie ihre $PREFIX Installation:"
TXT_auto_detect_patch_NO="$PREFIX wurde nicht auf ihrem Computer gefunden.\nWollen Sie trotzdem fortfahren und einen Patch von einer Liste wählen?"
TXT_version_type_in="Bitte geben Sie die Version ein, die Sie instalieren wollen:"
TXT_TypeInAnotherVersion="Geben Sie eine andere Version ein"

else # all other Languages or Englisch
TXT_downloadInstallPath_wait="$(eval_gettext 'Please wait ...')"
TXT_SetupFinished="$(eval_gettext '$TITLE has been successfully installed.')"
TXT_SelectSetupFile="$(eval_gettext 'Please select the setup file to run.')"
TXT_Fatal_NoPrefix="$(eval_gettext 'Error: No $PREFIX Wineprefix found. Have you $PREFIX correctly installed?')"
TXT_auto_detect_patch_BW_1="$(eval_gettext 'Starcraft and Starcraft: Brood War was found on your Computer.\nWould you like to install the newest Patch v')"
TXT_auto_detect_patch_BW_2="$(eval_gettext ' [yes] or would you like to choose the version of the patch from a list [no]?')"

TXT_auto_detect_patch_SC_1="$(eval_gettext 'Starcraft (without expansion) was found on your Computer.\nWould you like to install the newest Patch v')"
TXT_auto_detect_patch_SC_2="$(eval_gettext ' [yes] or would you like to choose the version of the patch from a list [no]?')"
TXT_choose_version_select="$(eval_gettext 'Please choose the version you want to install:')"
TXT_choose_version_select_SC="$(eval_gettext 'Please choose your $PREFIX installation:')"
TXT_auto_detect_patch_NO="$(eval_gettext '$PREFIX was not found on your Computer. \nWould you like to continue anyway and choose the version of the patch from a list?')"
TXT_version_type_in="$(eval_gettext 'Please type in the version you want to install:')"
TXT_TypeInAnotherVersion="$(eval_gettext 'Type in another version')"
fi

POL_System_TmpCreate "$PREFIX"

# needed:
# returns: Unix2Win_Path_RET
function Unix2Win_Path() { # $1= Unix Path: String $2= Prefix: String (default "z:\")

POL_Debug_Message "Unix path: $1"
POL_Debug_Message "returned path Prefix: $2"

if [ "$2" = "" ]
then
	Unix2Win_Path_RET="z:$(echo $1 | sed -e 's@\/\/@\/@g;s@\/@\x5c@g;')"
else
	Unix2Win_Path_RET="$2:$(echo $1 | sed -e 's@\/\/@\/@g;s@\/@\x5c@g;')"
fi

POL_Debug_Message "returend Win path: $Unix2Win_Path_RET"

}

# needed:
# returns: Win2Unix_Path_RET
function Win2Unix_Path() { # $1= Win Path: String $2= returend path Prefix: String (default "/")
POL_Debug_Message "Win path: $1"
POL_Debug_Message "returned path Prefix: $2"

if [ "$2" = "" ]

then # default prefix "/"
# sed commande changes \ to / and eliminates doubles // to /
	Win2Unix_Path_RET="$(echo "$1" | sed -e 's@[\x5c]@\/@g;s@[a-zA-Z]:@@g;s@\/\{1,6\}@\/@g;')"
	
else
	Win2Unix_Path_RET="$2$(echo "$1" | sed -e 's@[\x5c]@\/@g;s@[a-zA-Z]:@@g;s@\/\{1,6\}@\/@g;')"
fi

POL_Debug_Message "returend unix path: $Win2Unix_Path_RET"

}

# get Win Path of the Temp Dir
Unix2Win_Path "$POL_System_TmpDir"
POL_System_TmpDir_Win=$Unix2Win_Path_RET
POL_Debug_Message "POL_System_TmpDir_Win= $POL_System_TmpDir_Win"

# Gets the Data of a value in a Windows Registry Key
#
# needed: POL_System_TmpDir_Win, POL_System_TmpDir
# returns: getRegValue_RET
function getRegValue() { # $1=Value: String  $2=Registry Key: String

POL_Debug_Message "Value to get: $1"
POL_Debug_Message "Key: $2"

# read registry
cd $POL_System_TmpDir
POL_Wine regedit /e "$POL_System_TmpDir_Win\val_$1.reg" "$2"
POL_Debug_Message "Save Key to $POL_System_TmpDir_Win\val_$1.reg"

# first sed kommando gets the line the second extract the data and
# change \\ to \
getRegValue_RET=$(sed -n -e '/'$1'/p' "$POL_System_TmpDir/val_$1.reg" | sed -e 's@"'$1'"="\(.*\)".*@\1@g;s@[\x5c][\x5c]@\x5c@g;')
POL_Debug_Message "returend value data: $getRegValue_RET"
}

# Downloads the patch, install it and show the changelog
#
# needed: SC_InstallPath_Unix, POL_System_TmpDir, TITLE
# returns:
function downloadInstallPatch() { # $1= Installed Starcraft ("BW" or "SC"): String $2= Version to install: int
POL_SetupWindow_wait "$TXT_downloadInstallPath_wait" "$TITLE"

if [ "$1" = "BW" ]
	then
	# some versions have another Prefix
	if [ "$2" = "107" -o "$2" = "108" -o "$2" = "108b" -o "$2" = "109b" ]
		then
		Patch_Prefix="BW_"
		else # Standart Prefix
		Patch_Prefix="BW-"
	fi
	URL_Folder="http://ftp.blizzard.com/pub/broodwar/patches/PC/"
	
	elif [ "$1" = "SC" ]
	then
	# some versions have another Prefix
	if [ "$2" = "107" -o "$2" = "108" -o "$2" = "108b" -o "$2" = "109b" ]
		then
		Patch_Prefix="Star_"
		else # Standart Prefix
		Patch_Prefix="SC-"
	fi
	URL_Folder="http://ftp.blizzard.com/pub/starcraft/patches/PC/"
	
	else
	POL_Debug_Fatal "Unexpected first Function Parameter."
fi
POL_Debug_Message "Start download from http://ftp.blizzard.com/pub/ and install it."
cd "$POL_System_TmpDir"
POL_Download "${URL_Folder}${Patch_Prefix}${2}.exe"

POL_Wine start /unix "${POL_System_TmpDir}/${Patch_Prefix}${2}.exe"
# sed commando adds the points to the version number
POL_Wine_WaitExit "Patch v$(echo ${2} | sed -e 's@\([0-9]\)\([0-9]\{2\}\)\([0-9]\{1,50\}\)@\1\.\2\.\3@g;s@\([0-9]\)\([0-9]\{2\}[a-z]\)@\1\.\2@g;s@\([0-9]\)\([0-9]\{2\}\)@\1\.\2@g;')"

POL_SetupWindow_file "Changelog:" "$TITLE" "$SC_InstallPath_Unix/patch.txt"
} # downloadInstallPatch()

# check if all needed programms are instlled
check_one "sed" "sed"
check_one "wget" "wget"
POL_SetupWindow_Missing

POL_Wine_SelectPrefix $PREFIX

# check if the Prefix exists
if [ "$(POL_Wine_PrefixExists "$PREFIX")" = "False" ]
	then
		# The prefixe Starcraft doesn't exist
		POL_Debug_Fatal "$TXT_Fatal_NoPrefix"
	fi

POL_Wine_PrefixCreate

# gets the install Path of Starcraft from registry and sets varaibles
getRegValue "InstallPath" "HKEY_LOCAL_MACHINE\Software\Blizzard Entertainment\Starcraft"
SC_InstallPath_Win="$getRegValue_RET"
POL_Debug_Message "SC_InstallPath_Win=$SC_InstallPath_Win"

Win2Unix_Path "$SC_InstallPath_Win" "$WINEPREFIX/drive_c"
SC_InstallPath_Unix="$Win2Unix_Path_RET"
POL_Debug_Message "SC_InstallPath_Unix=$SC_InstallPath_Unix"


# Let the user choose the version which he wants to install and
# runs downloadInstallPatch()
#
# requires: prepare_patch_list() schuld run before
# needed: version_array,downloadInstallPatch(), TITLE
# returns:
function choose_version() { # no values

	POL_SetupWindow_menu_num "$TXT_choose_version_select_SC" "$TITLE" "Starcraft~Starcraft: Brood War" "~"

if [ $APP_ANSWER = 0 ]
then
	SC_Installed="SC"
else
	SC_Installed="BW"
fi

	POL_SetupWindow_menu_num "$TXT_choose_version_select" "$TITLE" "$version_vector" "~"

if [ $APP_ANSWER = 0 ]
then # user wants to type in the version
	POL_Debug_Message "PLease refer to http://ftp.blizzard.com/pub/starcraft/broodwar/PC/ for possible downloads."
	POL_SetupWindow_textbox "$TXT_version_type_in" "$TITLE" "$version_max_txt"
	POL_Debug_Message "input version: $APP_ANSWER"

	downloadInstallPatch $SC_Installed "$(echo "$APP_ANSWER" | sed -e 's/[\.\ ]//g')"

else
	downloadInstallPatch $SC_Installed "${version_array[$APP_ANSWER]}"
fi

} # choose_version()


# detect the installed starcraft and offers the user to
# automatically install the newest patch
#
# requires: prepare_patch_list()
# needed: SC_InstallPath_Unix,downloadInstallPatch(),choose_version(),
#	  version_max,version_max_txt, TITLE
# returns:
function auto_detect_patch() {

	cd "$SC_InstallPath_Unix"

if [ -f "${SC_InstallPath_Unix}BroodWar.mpq" ] # for Brood War
then
	POL_SetupWindow_question "${TXT_auto_detect_patch_BW_1}${version_max_txt}${TXT_auto_detect_patch_BW_2}" "$TITLE"
if [ $APP_ANSWER = TRUE ]
	then
	downloadInstallPatch "BW" "${version_max}"
	else
	choose_version
fi

elif [ -f "${SC_InstallPath_Unix}StarCraft.exe" ] # for Starcraft (without expansion)
then
POL_SetupWindow_question "${TXT_auto_detect_patch_SC_1}${version_max_txt}${TXT_auto_detect_patch_SC_2}" "$TITLE"
if [ $APP_ANSWER = TRUE ]
	then
	downloadInstallPatch "SC" "${version_max}"
	else
	choose_version
fi

else # no file was found. Perhaps some path variables are wrong.
	POL_SetupWindow_question "$TXT_auto_detect_patch_NO" "$TITLE"
if [ $APP_ANSWER = TRUE ]
	then
	choose_version
fi

fi # -f "$SC_InstallPath_Unix/BroodWar.mpq"

} # auto_detect_patch()


# Downloads the available patches and sets the related variables
#
# requires: 
# needed: SC_InstallPath_Unix,downloadInstallPatch(),choose_version(),
#	  version_max,version_max_txt, TITLE
# returns: version_max, version_vector, version_max_txt, version_array
function prepare_patch_list() { # no values

	cd "$POL_System_TmpDir"
	wget "http://ftp.blizzard.com/pub/broodwar/patches/PC/"


if [ "$(cat index.html)" = "" -o ! -f index.html ]
then #if download was not sucessful
	POL_Debug_Error "Download of Patch List not sucessful."
	POL_SetupWindow_menu_num "$TXT_choose_version_select_SC" "$TITLE" "Starcraft~Starcraft: Brood War" "~"

if [ $APP_ANSWER = 0 ]
then
	SC_Installed="SC"
else
	SC_Installed="BW"
fi
	POL_SetupWindow_textbox "$TXT_version_type_in" "$TITLE" "1.16.1"
	downloadInstallPatch "SC_Installed" "$(echo "$APP_ANSWER" | sed -e 's/\.//g')"

else # all ok

# gets the correct lines with patches
sed -n -e '/"BW[-_][0-9]*[a-z]\{0,1\}\.exe"/p' index.html  > version_lines.txt
# gets the versions numbers
sed -e 's/.*"BW[-_]\([0-9]*[a-z]\{0,1\}\)\.exe".*/\1/g' version_lines.txt > version_list.txt

i=1 # array index
version_max=0
version_vector="$TXT_TypeInAnotherVersion"

# processed the list to the variables
for var in `cat version_list.txt`
do
version_array[$i]="$var"

# list of versions seperated with ~ for POL_SetupWindow_menu_num
# sed commando adds dots to the version numbers
version_vector="${version_vector}~v$(echo ${var} | sed -e 's@\([0-9]\)\([0-9]\{2\}\)\([0-9]\{1,50\}\)@\1\.\2\.\3@g;s@\([0-9]\)\([0-9]\{2\}[a-z]\)@\1\.\2@g;s@\([0-9]\)\([0-9]\{2\}\)@\1\.\2@g;')"

# look for the newest patch version
if [[ "$var" > "$version_max" ]]
	then 
	version_max=$var
fi

i=$i+1
done

# version_max with dots for viewing
version_max_txt="$(echo ${version_max} | sed -e 's@\([0-9]\)\([0-9]\{2\}\)\([0-9]\{1,50\}\)@\1\.\2\.\3@g;s@\([0-9]\)\([0-9]\{2\}[a-z]\)@\1\.\2@g;s@\([0-9]\)\([0-9]\{2\}\)@\1\.\2@g;')"
POL_Debug_Message "version_max_txt= $version_max_txt"

# starts installing
auto_detect_patch

fi # ! -f index.html

} # prepare_patch_list()


POL_SetupWindow_InstallMethod "LOCAL,DOWNLOAD"
if [ $INSTALL_METHOD = "LOCAL" ]
	then
	POL_SetupWindow_browse "$TXT_SelectSetupFile" "$TITLE"
	
	POL_Wine start /unix "$APP_ANSWER"
	POL_Wine_WaitExit "Patch"
	
	POL_SetupWindow_file "Changelog:" "$TITLE" "$SC_InstallPath_Unix/patch.txt"


elif [ $INSTALL_METHOD = "DOWNLOAD" ]
	then
	prepare_patch_list

else
	POL_Debug_Fatal "Error: Unexpected Return value of the InstallMethod Window."

fi

POL_SetupWindow_message "$TXT_SetupFinished" "$TITLE"
POL_System_TmpDelete
POL_SetupWindow_Close

exit 
