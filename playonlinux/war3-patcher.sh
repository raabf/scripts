#!/bin/bash
#
# Only For : http://www.playonlinux.com
# Date : (2012-05-30 22-00)
# Last revision : (2012-05-30 22-00)
# Wine version used : 1.5.3
# Distribution used to test : Ubuntu 12.04 LTS
# Author : Amyros 
# E-Mail : amyros@live.de

[ "$PLAYONLINUX" = "" ] && exit 0
source "$PLAYONLINUX/lib/sources"

TITLE="Warcraft III: Reign of Chaos and The Frozen Throne - Patcher"
PREFIX="WarcraftIII"

war3_InstallPath_Std_Win="c:\Program Files\Warcraft III\\"

POL_SetupWindow_Init
POL_Debug_Init

POL_SetupWindow_presentation "$TITLE" "Blizzard" "http://www.blizzard.com/games/sc/" "Amyros <amyros@live.de>" "$PREFIX"


if [ "$POL_LANG" == "de" ]

then # German translation
TXT_downloadInstallPath_wait="Bitte warten ..."
TXT_SetupFinished="$TITLE wurde erfolgreich installiert."
TXT_SelectSetupFile="Bitte wählen sie die Installations Datei, die sie ausführen wollen:"
TXT_Fatal_NoPrefix="Fehler: Kein $PREFIX Wineprefix gefunden. Haben Sie $PREFIX korrekt installiert?"
TXT_auto_detect_patch_BW_1="Starcraft und Starcraft: Brood War wurde auf ihrem Computer gefunden.\nWollen sie den aktuellsten Patch v"
TXT_auto_detect_patch_BW_2=" installieren [Ja] oder wollen Sie die Version des Patches selbst von einer Liste wählen [Nein]?"

TXT_auto_detect_patch_SC_1="Starcraft (ohne Erweiterungen) wurde auf ihrem Computer gefunden.\nWollen sie den aktuellsten Patch v"
TXT_auto_detect_patch_SC_2=" installieren [Ja] oder wollen Sie die Version des Patches selbst von einer Liste wählen [Nein]?"
TXT_choose_version_select="Bitte wählen Sie die Version, die Sie installieren möchten:"
TXT_choose_version_select_war3="Bitte wählen Sie ihre $PREFIX Installation:"
TXT_auto_detect_patch_NO="$PREFIX wurde nicht auf ihrem Computer gefunden.\nWollen Sie trotzdem fortfahren und einen Patch von einer Liste wählen?"
TXT_version_type_in="Bitte geben Sie die Version ein, die Sie instalieren wollen:"
TXT_TypeInAnotherVersion="Geben Sie eine andere Version ein"
TXT_from="$(eval_gettext 'von')"
TXT_to="$(eval_gettext 'nach')"

else # all other Languages or Englisch
TXT_downloadInstallPath_wait="$(eval_gettext 'Please wait ...')"
TXT_SetupFinished="$(eval_gettext '$TITLE has been successfully installed.')"
TXT_SelectSetupFile="$(eval_gettext 'Please select the setup file to run.')"
TXT_Fatal_NoPrefix="$(eval_gettext 'Error: No $PREFIX Wineprefix found. Have you $PREFIX correctly installed?')"
TXT_auto_detect_patch_BW_1="$(eval_gettext 'Starcraft and Starcraft: Brood War was found on your Computer.\nWould you like to install the newest Patch v')"
TXT_auto_detect_patch_BW_2="$(eval_gettext ' [yes] or would you like to choose the version of the patch from a list [no]?')"

TXT_auto_detect_patch_SC_1="$(eval_gettext 'Starcraft (without expentions) was found on your Computer.\nWould you like to install the newest Patch v')"
TXT_auto_detect_patch_SC_2="$(eval_gettext ' [yes] or would you like to choose the version of the patch from a list [no]?')"
TXT_choose_version_select="$(eval_gettext 'Please choose the version you want to install:')"
TXT_choose_version_select_war3="$(eval_gettext 'Please choose your $PREFIX installation:')"
TXT_auto_detect_patch_NO="$(eval_gettext '$PREFIX was not found on your Computer. \nWould you like to continue anyway and choose the version of the patch from a list?')"
TXT_version_type_in="$(eval_gettext 'Please type in the version you want to install:')"
TXT_TypeInAnotherVersion="$(eval_gettext 'Type in another version')"
TXT_from="$(eval_gettext 'from')"
TXT_to="$(eval_gettext 'to')"
fi
TXT_choose_lang="$(eval_gettext 'Please choose your $PREFIX Language:')"
war3_lang_vector="Castellano~Cesky~Chinese (Simplified)~Chinese (Traditional)~Deutsch~Englisch~Francais~Italiano~Japanese~Korean~Polski~Russian"
war3_lang_array=(Castellano Cesky Chinese_Simp Chinese_Trad Deutsch Englisch Francais Italiano Japanese Korean Polski Russian)

POL_System_TmpCreate "$PREFIX"

# needed:
# returns: Unix2Win_Path_RET
function Unix2Win_Path() { # $1= Unix Path: String $2= Prefix: String (default "z:\")

POL_Debug_Message "Unix path: $1"
POL_Debug_Message "returned path Prefix: $2"

if [ "$2" = "" ]
then
	Unix2Win_Path_RET="z:$(echo $1 | sed -e 's@\/\/@\/@g;s@\/@\x5c@g;')"
else
	Unix2Win_Path_RET="$2:$(echo $1 | sed -e 's@\/\/@\/@g;s@\/@\x5c@g;')"
fi

POL_Debug_Message "returend Win path: $Unix2Win_Path_RET"

}

# needed:
# returns: Win2Unix_Path_RET
function Win2Unix_Path() { # $1= Win Path: String $2= returend path Prefix: String (default "/")
POL_Debug_Message "Win path: $1"
POL_Debug_Message "returned path Prefix: $2"

if [ "$2" = "" ]

then # default prefix "/"
# sed commande changes \ to / and eliminates doubles // to /
	Win2Unix_Path_RET="$(echo "$1" | sed -e 's@[\x5c]@\/@g;s@[a-zA-Z]:@@g;s@\/\{1,6\}@\/@g;')"
	
else
	Win2Unix_Path_RET="$2$(echo "$1" | sed -e 's@[\x5c]@\/@g;s@[a-zA-Z]:@@g;s@\/\{1,6\}@\/@g;')"
fi

POL_Debug_Message "returend unix path: $Win2Unix_Path_RET"

}

# get Win Path of the Temp Dir
Unix2Win_Path "$POL_System_TmpDir"
POL_System_TmpDir_Win=$Unix2Win_Path_RET
POL_Debug_Message "POL_System_TmpDir_Win= $POL_System_TmpDir_Win"

# Gets the Data of a value in a Windows Registry Key
#
# needed: POL_System_TmpDir_Win, POL_System_TmpDir
# returns: getRegValue_RET
POL_Debug_Message "Start getRegValue()"
function getRegValue() { # $1=Value: String  $2=Registry Key: String $3= Standart value on fail

POL_Debug_Message "Value to get: $1"
POL_Debug_Message "Key: $2"

# read registry
cd $POL_System_TmpDir
POL_Wine regedit /e "$POL_System_TmpDir_Win\val_$1.reg" "$2"
if [ "$(cat val_$1.reg)" = "" -o ! -f "val_$1.reg" ]

then #if key was not sucessful exported
	POL_Debug_Error "$PREFIX registry key for reading \"$1\" was not sucessful exported! Is $PREFIX correctly installed? Standart value ($3) is used."

getRegValue_RET=$war3_InstallPath_Std_Win
POL_Debug_Message "returend value data (standart value): $getRegValue_RET"

else
POL_Debug_Message "Save Key to $POL_System_TmpDir_Win\val_$1.reg"

# first sed kommando gets the line the second extract the data and
# change \\ to \
getRegValue_RET=$(sed -n -e '/'$1'/p' "$POL_System_TmpDir/val_$1.reg" | sed -e 's@"'$1'"="\(.*\)".*@\1@g;s@[\x5c][\x5c]@\x5c@g;')
POL_Debug_Message "returend value data: $getRegValue_RET"

fi
}

# Downloads the patch, install it and show the changelog
#
# needed: war3_InstallPath_Unix, POL_System_TmpDir, TITLE
# returns:
POL_Debug_Message "Start downloadInstallPatch()"
function downloadInstallPatch() { # $1= Installed WarcraftIII ("ROC" or "TFT"): String $2= Version to install: int $3= Language: String
POL_SetupWindow_wait "$TXT_downloadInstallPath_wait" "$TITLE"

if [ "$1" = "TFT" ]
	then
	
	Patch_Prefix="War3TFT"
	URL_Folder="http://ftp.blizzard.com/pub/war3x/patches/PC/"
	
	elif [ "$1" = "ROC" ]
	then
	
	Patch_Prefix="War3ROC"
	URL_Folder="http://ftp.blizzard.com/pub/war3/patches/PC/"
	
	else
	POL_Debug_Fatal "Unexpected first Function Parameter."
fi
POL_Debug_Message "Start download from http://ftp.blizzard.com/pub/ and install it."
cd "$POL_System_TmpDir"
POL_Download "${URL_Folder}${Patch_Prefix}_${2}_${3}.exe"

POL_Wine start /unix "${POL_System_TmpDir}/${Patch_Prefix}${2}.exe"
# sed commando adds the points to the version number
POL_Wine_WaitExit "Patch $(echo ${2} | sed -e 's/\([0-9]\)\([0-9a-z]\{1,5\}\)/\1\.\2/g');s/\([0-9]\)\([0-9a-z]\{1,4\}\)_\([0-9]\)\([0-9a-z]\{1,4\}\)/'$TXT_from' v\1\.\2 '$TXT_to' v\3\.\4/g;')"

POL_SetupWindow_file "Changelog:" "$TITLE" "$war3_InstallPath_Unix/Patch.txt"
} # downloadInstallPatch()

# check if all needed programms are instlled
check_one "sed" "sed"
check_one "wget" "wget"
POL_SetupWindow_Missing

POL_Wine_SelectPrefix $PREFIX

# check if the Prefix exists
if [ "$(POL_Wine_PrefixExists "$PREFIX")" = "False" ]
	then
		# The prefixe WarcraftIII doesn't exist
		POL_Debug_Fatal "$TXT_Fatal_NoPrefix"
	fi

POL_Wine_PrefixCreate

# gets the install Path of WarcraftIII from registry and sets varaibles
getRegValue "InstallPath" "HKEY_LOCAL_MACHINE\Software\Blizzard Entertainment\Warcraft III" "$war3_InstallPath_Std_Win"
war3_InstallPath_Win="$getRegValue_RET"
POL_Debug_Message "war3_InstallPath_Win=$war3_InstallPath_Win"

Win2Unix_Path "$war3_InstallPath_Win" "$WINEPREFIX/drive_c"
war3_InstallPath_Unix="$Win2Unix_Path_RET"
POL_Debug_Message "war3_InstallPath_Unix=$war3_InstallPath_Unix"


# Let the user choose the version which he wants to install and
# runs downloadInstallPatch()
#
# requires: prepare_patch_list() schuld run before
# needed: version_array,downloadInstallPatch(), TITLE
# returns:
POL_Debug_Message "Start choose_version()"
function choose_version() { # no values

	POL_SetupWindow_menu_num "$TXT_choose_version_select_war3" "$TITLE" "Warcraft III: Reign of Chaos~Warcraft III: Reign of Chaos + The Frozen Throne" "~"

if [ $APP_ANSWER = 0 ]
then
	war3_Installed="ROC"
else
	war3_Installed="TFT"
fi

	POL_SetupWindow_menu_num "$TXT_choose_version_select" "$TITLE" "$version_vector" "~"

if [ $APP_ANSWER = 0 ]
then # user wants to type in the version
	POL_Debug_Message "PLease refer to http://ftp.blizzard.com/pub/war3/patches/pc/ for possible downloads."
	POL_SetupWindow_textbox "$TXT_version_type_in" "$TITLE" "$version_max_txt"
	POL_Debug_Message "input version: $APP_ANSWER"

	downloadInstallPatch $war3_Installed "$(echo "$APP_ANSWER" | sed -e 's/[\.\ ]//g')" ${war3_lang_array[$war3_selected_lang]}

else
	downloadInstallPatch $war3_Installed "${version_array[$APP_ANSWER]}" ${war3_lang_array[$war3_selected_lang]}
fi

} # choose_version()


# detect the installed WarcraftIII and offers the user to
# automatically install the newest patch
#
# requires: prepare_patch_list()
# needed: war3_InstallPath_Unix,downloadInstallPatch(),choose_version(),
#	  version_max,version_max_txt, TITLE
# returns:
POL_Debug_Message "start auto_detect_patch()"
function auto_detect_patch() {

	cd "$war3_InstallPath_Unix"

if [ -f "${war3_InstallPath_Unix}Frozen Throne.exe" ] # for TFT
then
	POL_SetupWindow_question "${TXT_auto_detect_patch_BW_1}${version_max_txt}${TXT_auto_detect_patch_BW_2}" "$TITLE"
if [ $APP_ANSWER = TRUE ]
	then
	downloadInstallPatch "TFT" "${version_max}"  ${war3_lang_array[$war3_selected_lang]}
	else
	choose_version
fi

elif [ -f "${war3_InstallPath_Unix}war3.exe" ] # for WarcraftIII ROC
then
POL_SetupWindow_question "${TXT_auto_detect_patch_SC_1}${version_max_txt}${TXT_auto_detect_patch_SC_2}" "$TITLE"
if [ $APP_ANSWER = TRUE ]
	then
	downloadInstallPatch "ROC" "${version_max}" ${war3_lang_array[$war3_selected_lang]}
	else
	choose_version
fi

else # no file was found. Perhaps some path variables are wrong.
	POL_SetupWindow_question "$TXT_auto_detect_patch_NO" "$TITLE"
if [ $APP_ANSWER = TRUE ]
	then
	choose_version
fi

fi # -f "$war3_InstallPath_Unix/BroodWar.mpq"

} # auto_detect_patch()


# Downloads the available patches and sets the related variables
#
# requires: 
# needed: war3_InstallPath_Unix,downloadInstallPatch(),choose_version(),
#	  version_max,version_max_txt, TITLE
# returns: version_max, version_vector, version_max_txt, version_array
POL_Debug_Message "start prepare_patch_list()"
function prepare_patch_list() { # no values

	cd "$POL_System_TmpDir"
	wget "http://ftp.blizzard.com/pub/war3x/patches/pc/"


if [ "$(cat index.html)" = "" -o ! -f index.html ]
then #if download was not sucessful
	POL_Debug_Error "Download of Patch List not sucessful."
#	POL_SetupWindow_menu_num "$TXT_choose_version_select_war3" "$TITLE" "Warcraft III: Reign of Chaos~Warcraft III: Reign of Chaos + The Frozen Throne" "~"

#if [ $APP_ANSWER = 0 ]
#then
#	war3_Installed="ROC"
#else
#	war3_Installed="TFT"
#fi

#	POL_SetupWindow_textbox "$TXT_version_type_in" "$TITLE" "1.16.1"
#	downloadInstallPatch "$war3_Installed" "$(echo "$APP_ANSWER" | sed -e 's/\.//g')" ${war3_lang_array[$war3_selected_lang]}

else # all ok
#War3TFT_124a_124b_English.exe
# gets the correct lines with patches
sed -n -e '/"War3TFT_[a-z0-9_]*_Englisch\.exe"/p' index.html  > version_lines.txt
# gets the versions numbers
sed -e 's/.*"War3TFT_\([a-z0-9_]*\)_Englisch\.exe".*/\1/g' version_lines.txt > version_list.txt

i=1 # array index
version_max=0
version_vector="$TXT_TypeInAnotherVersion"

# processed the list to the variables
for var in `cat version_list.txt`
do
version_array[$i]="$var"

# list of versions seperated with ~ for POL_SetupWindow_menu_num
# sed commando adds dots to the version numbers
version_vector="${version_vector}~$(echo ${var} | sed -e 's/\([0-9]\)\([0-9a-z]\{1,5\}\)/v\1\.\2/g;s/\([0-9]\)\([0-9a-z]\{1,4\}\)_\([0-9]\)\([0-9a-z]\{1,4\}\)/'${TXT_from}' v\1\.\2 '${TXT_to}' v\3\.\4/g;)"

# look for the newest patch version
#if [ $(echo $var | sed -e 's/[A-Za-z0-9\-\.]//g') != "_" ]
#then
#if [[ "$var" > "$version_max" ]]
#	then 
#	version_max=$var
#fi
#fi
version_max="126a"
i=$i+1
done

POL_Debug_Message "version_vector= $version_vector"

# version_max with dots for viewing
version_max_txt="$(echo ${version_max} | sed -e 's/\([0-9]\)\([0-9a-z]\{1,5\}\)/\1\.\2/g')"
POL_Debug_Message "version_max_txt= $version_max_txt"

# starts installing
auto_detect_patch

fi # ! -f index.html

} # prepare_patch_list()


POL_SetupWindow_InstallMethod "LOCAL,DOWNLOAD"
if [ $INSTALL_METHOD = "LOCAL" ]
	then
	POL_SetupWindow_browse "$TXT_SelectSetupFile" "$TITLE"
	
	POL_Wine start /unix "$APP_ANSWER"
	POL_Wine_WaitExit "Patch"
	
	POL_SetupWindow_file "Changelog:" "$TITLE" "$war3_InstallPath_Unix/Patch.txt"


elif [ $INSTALL_METHOD = "DOWNLOAD" ]
	then
	
	POL_SetupWindow_menu_num "$TXT_choose_lang" "$TITLE" "$war3_lang_vector" "~"
	war3_selected_lang=$APP_ANSWER
	
	prepare_patch_list

else
	POL_Debug_Fatal "Error: Unexpected Return value of the InstallMethod Window."

fi

POL_SetupWindow_message "$TXT_SetupFinished" "$TITLE"
POL_System_TmpDelete
POL_SetupWindow_Close

exit 
