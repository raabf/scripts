#!/bin/bash
#############################################################
## Title: gitea-update
## Abstact: Updates gitea.
## Author:  Fabian Raab <fabian@raab.link>
## Dependencies: wget, gpg2
## Creation Date: 2010-12-28
## Last Edit: 2018-12-28
##############################################################


SCRIPTNAME=$(basename $0)
SCRIPTPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
EXIT_SUCCESS=0
EXIT_FAILURE=1
EXIT_ERROR=2
EXIT_BUG=10

URL_BASE="https://dl.gitea.io/gitea/"
BIN_DIR="/usr/local/bin/"


##### Colors #####
RCol='\e[0m'    # Text Reset

# Regular           Bold                Underline           High Intensity      BoldHigh Intens     Background          High Intensity Backgrounds
Bla='\e[0;30m';     BBla='\e[1;30m';    UBla='\e[4;30m';    IBla='\e[0;90m';    BIBla='\e[1;90m';   On_Bla='\e[40m';    On_IBla='\e[0;100m';
Red='\e[0;31m';     BRed='\e[1;31m';    URed='\e[4;31m';    IRed='\e[0;91m';    BIRed='\e[1;91m';   On_Red='\e[41m';    On_IRed='\e[0;101m';
Gre='\e[0;32m';     BGre='\e[1;32m';    UGre='\e[4;32m';    IGre='\e[0;92m';    BIGre='\e[1;92m';   On_Gre='\e[42m';    On_IGre='\e[0;102m';
Yel='\e[0;33m';     BYel='\e[1;33m';    UYel='\e[4;33m';    IYel='\e[0;93m';    BIYel='\e[1;93m';   On_Yel='\e[43m';    On_IYel='\e[0;103m';
Blu='\e[0;34m';     BBlu='\e[1;34m';    UBlu='\e[4;34m';    IBlu='\e[0;94m';    BIBlu='\e[1;94m';   On_Blu='\e[44m';    On_IBlu='\e[0;104m';
Pur='\e[0;35m';     BPur='\e[1;35m';    UPur='\e[4;35m';    IPur='\e[0;95m';    BIPur='\e[1;95m';   On_Pur='\e[45m';    On_IPur='\e[0;105m';
Cya='\e[0;36m';     BCya='\e[1;36m';    UCya='\e[4;36m';    ICya='\e[0;96m';    BICya='\e[1;96m';   On_Cya='\e[46m';    On_ICya='\e[0;106m';
Whi='\e[0;37m';     BWhi='\e[1;37m';    UWhi='\e[4;37m';    IWhi='\e[0;97m';    BIWhi='\e[1;97m';   On_Whi='\e[47m';    On_IWhi='\e[0;107m';


###### Functions ######

function usage #(exit_code: Optional) 
{
echo -e "${BRed}Usage: ${Yel}$SCRIPTNAME${RCol} ${UGre}version${RCol}

    Updates gitea to ${UGre}version${RCol}. Does not uninstall old versions.

${BRed}EXAMPLES:${RCol}
    ${Yel}$SCRIPTNAME${RCol} ${Gre}1.6.2${RCol}
    
${BRed}EXIT STATUS:${RCol}
    If everything is successfull the script will exit with $EXIT_SUCCESS.
    Failure exit statuses of the script itself are $EXIT_FAILURE, $EXIT_ERROR, and $EXIT_BUG.
"
    [[ $# -eq 1 ]] && exit $1 || exit $EXIT_FAILURE
}

if [[ $# -ne 1 ]]; then
    usage
fi

version="$1"
filename="gitea-${version}-linux-amd64"

full_url="${URL_BASE}/${version}/${filename}"
filepath="${BIN_DIR}/${filename}"

if [ ! -f "${filepath}" ]; then
    wget -O "${filepath}" "${full_url}" || exit $?
fi

wget -O "${filepath}.sha256" "${full_url}.sha256" || exit $?
wget -O "${filepath}.asc" "${full_url}.asc" || exit $?

cd "${BIN_DIR}" || exit $?
sha256sum --check "${filepath}.sha256" || exit $?

gpg2 --verify "${filepath}.asc" "${filepath}" || exit $?

rm --verbose "${filepath}.asc" "${filepath}.sha256" || exit $?

chmod a+x "${filepath}" || exit $?

ln --symbolic --force --relative --verbose "${filepath}" "${BIN_DIR}/gitea" || exit $?

exit $EXIT_SUCCESS

