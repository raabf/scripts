Gitea Update
============

Gitea Homepage: <https://gitea.io>

Installs and updates gitea binary.

Run `gitea-update.sh` to see its usage.
