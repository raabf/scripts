#!/bin/zsh

0=${(%):-%N}
RAABF_SCRIPTS_ROOT="${0:A:h}"

langtool()
{
    # `bash -c ...` manipulates $0 so that it contain `langtool`
    # instead of `langtool.sh`
    bash -c ". ${RAABF_SCRIPTS_ROOT}/LanguageTool/langtool.sh" langtool ${@}
}    

gitea-update()
{
    bash -c ". ${RAABF_SCRIPTS_ROOT}/gitea_update/gitea-update.sh" gitea-update ${@}
}    


